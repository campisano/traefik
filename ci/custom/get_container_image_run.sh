#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

IMAGE="docker.io/traefik:v3.2.1"

echo "${IMAGE}"
