APP_NAME := $(shell ./ci/custom/get_project_name.sh)
FROM_IMAGE := $(shell ./ci/custom/get_container_image_run.sh)

.PHONY: up
up:
	docker-compose --file docker/docker-compose.yml up --detach --force-recreate --renew-anon-volumes

.PHONY: down
down:
	docker-compose --file docker/docker-compose.yml down --volumes

.PHONY: logs
logs:
	docker-compose --file docker/docker-compose.yml logs --follow

.PHONY: update
update: image
	docker-compose --file docker/docker-compose.yml stop $(APP_NAME) || true
	docker-compose --file docker/docker-compose.yml rm -f -v $(APP_NAME) || true
	docker-compose --file docker/docker-compose.yml up --detach --force-recreate --renew-anon-volumes $(APP_NAME)

.PHONY: image
image:
	docker build --tag $(APP_NAME):local --file ci/custom/Dockerfile_amd64 --build-arg "FROM_IMAGE=$(FROM_IMAGE)" .
